module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');
    let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    const sensorId = (req.query.sensorId || (req.body && req.body.sensorId));
    const sensorValue = (req.query.value || (req.body && req.body.value));
    let xhr = new XMLHttpRequest();
    xhr.open('PATCH', 'http://localhost/api/sensors/' + sensorId + '?value=' + sensorValue, true)
    xhr.send()

    const responseMessage = sensorId
        ? "Hello, sensor number " + sensorId + " with measured value of " + sensorValue + ". This HTTP triggered function executed successfully. Your data will be sent to the database immediately!"
        : "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.";

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage
    };
}
